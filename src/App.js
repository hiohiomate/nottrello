import logo from './logo.svg';
import './App.css';
import { Trello } from './components/Trello';

function App() {
  return (
    <Trello/>
  );
}

export default App;
