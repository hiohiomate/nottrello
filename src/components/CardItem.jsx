import _ from "lodash";
import { useContext, useEffect, useState } from "react";
import { CardContext, ColumnContext } from "./Context";

export default function CardItem(props) {
  const { cards, setCards } = useContext(CardContext);
  const columns = useContext(ColumnContext);
  const [editMode, setEditMode] = useState(false);
  const [title, setTitle] = useState(props.title);
  const [description, setDescription] = useState(props.description);

  function moveColumn(index) {
    const tempCards = [...cards];
    tempCards[props.id] = {
      id: props.id,
      title: props.title,
      description: props.description,
      column: props.column + index,
    };
    setCards(tempCards);
  }

  function toggleField(title, setTitle, description, setDescription) {
    if (!editMode)
      return (
        <div onClick={toggleEditMode}>
          <div style={({ overflow: "hidden" }, { width: "inherit" })}>
            {title}
          </div>
          <div style={{ overflow: "hidden" }}>{description}</div>
        </div>
      );
    else
      return (
        <div>
          <form onSubmit={handleSubmit}>
            <input
              type="text"
              value={title}
              required
              onChange={(e) => setTitle(e.target.value)}
            />
            <textarea
              style={{ resize: "none" }}
              value={description}
              required
              onChange={(e) => setDescription(e.target.value)}
            >
              {description}
            </textarea>
            <input type="submit" value="Update ticket" />
          </form>
        </div>
      );
  }

  useEffect(() => console.log(columns));

  function handleSubmit(event) {
    event.preventDefault();

    const tempCards = [...cards];
    tempCards[props.id] = {
      id: props.id,
      title: title,
      description: description,
      column: props.column,
    };
    setCards(tempCards);

    toggleEditMode();
  }

  function toggleEditMode() {
    setEditMode(!editMode);
  }

  return (
    <div style={{ display: "flex" }}>
      {props.column > 0 ? (
        <button
          style={{ width: "10%" }}
          onClick={() => {
            moveColumn(-1);
          }}
        >
          {"<"}
        </button>
      ) : (
        <div style={{ width: "10%" }} />
      )}
      <div style={{ width: "80%" }}>
        {toggleField(title, setTitle, description, setDescription)}
      </div>
      {props.column < Object.keys(columns).length - 1 ? (
        <button
          style={{ width: "10%" }}
          onClick={() => {
            moveColumn(1);
          }}
        >
          {">"}
        </button>
      ) : (
        <div style={{ width: "10%" }} />
      )}
    </div>
  );
}
