import _ from "lodash";
import { useContext, useEffect } from "react";
import { CardContext, ColumnContext } from "./Context";
import "./Trello.css";
import CardItem from "./CardItem";

export default function CardColumn(props) {
  const { cards, setCards } = useContext(CardContext);
  const filteredCards = _.filter(cards, { column: props.id });

  useEffect(() => {
    localStorage.setItem("myCards", JSON.stringify(cards));
  });

  function addCard() {
    setCards((prevCards) => {
      return [
        ...prevCards,
        {
          id: prevCards.length,
          title: "Title",
          description: "Description",
          column: props.id,
        },
      ];
    });
  }

  return (
    <div className="column">
      <h3>{props.title}</h3>
      {filteredCards.map((item) => (
        <CardItem
          key={item.id}
          id={item.id}
          title={item.title}
          description={item.description}
          column={item.column}
        />
      ))}
      <button style={{ width: "inherit" }} onClick={addCard}>
        Create new ticket
      </button>
    </div>
  );
}
