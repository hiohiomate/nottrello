import { useEffect, useState } from "react";
import CardColumn from "./CardColumn";
import { ColumnContext } from "./Context";

export default function CardRow(props) {
  const [columns, setColumns] = useState(props.columns);

  useEffect(() => {
    console.log(columns);
    localStorage.setItem("columns", JSON.stringify(columns));
  });

  function addColumn() {
    setColumns([
      ...columns,
      {
        title: "Row",
        id: columns.length,
      },
    ]);
  }

  return (
    <ColumnContext.Provider value={columns}>
      <div className="row">
        {columns.map((column) => (
          <CardColumn key={column.id} id={column.id} title={column.title} />
        ))}

        <div className="column">
          <button onClick={addColumn} style={{ width: "inherit" }}>
            Create new column
          </button>
        </div>
      </div>
    </ColumnContext.Provider>
  );
}
