import _ from "lodash";
import { useState } from "react";
import { CardContext } from "./Context";
import "./Trello.css";
import CardRow from "./CardRow";

export function Trello() {
  const [cards, setCards] = useState(() => {
    const cardData = localStorage.getItem("myCards");
    if (cardData) {
      return JSON.parse(cardData);
    } else return [];
  });

  function getColumnData() {
    const columnData = localStorage.getItem("columns");
    if (columnData) {
      return JSON.parse(columnData);
    } else return [];
  }

  return (
    <CardContext.Provider value={{ cards, setCards }}>
      <CardRow columns={getColumnData} />
    </CardContext.Provider>
  );
}
